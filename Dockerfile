# Build stage
FROM golang as build
ARG APP_VERSION
COPY ./src $GOPATH/src/app
RUN go get gopkg.in/mgo.v2
RUN CGO_ENABLED=0 \
    GOOS=linux \
    go build \
    -a \
    -installsuffix cgo \
    -ldflags "-X main.appVersion=$APP_VERSION" \
    -o /main \
    $GOPATH/src/app/main.go

FROM scratch
COPY --from=build /main /
EXPOSE 8080
CMD ["/main"]
