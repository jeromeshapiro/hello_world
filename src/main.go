package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"gopkg.in/mgo.v2"
)

type AppInfo struct {
	Name     string `json:"name"`
	Version  string `json:"version"`
	PodId    string `json:"pod_id"`
	ApiKey   string `json:"api_key"`
	LogCount int    `json:"log_count"`
}

type Event struct {
	Message string
}

const appName = "Hello, Dimebox!"

var (
	appVersion string
	podId      string
	apiKey     string
)

func logEvent() (count int, err error) {
	session, err := mgo.Dial(os.Getenv("MONGO_URL"))

	if err != nil {
		return 0, err
	}

	defer session.Close()

	collection := session.DB("test").C("event")

	err = collection.Insert(&Event{"Incoming API request"})

	if err != nil {
		return 0, err
	}

	count, err = collection.Count()

	if err != nil {
		return 0, err
	}

	return count, nil
}

func resetLogEvents() (err error) {
	session, err := mgo.Dial(os.Getenv("MONGO_URL"))

	if err != nil {
		return err
	}

	defer session.Close()

	collection := session.DB("test").C("event")

	err = collection.DropCollection()

	if err != nil {
		return err
	}

	return nil
}

func handler(w http.ResponseWriter, r *http.Request) {
	logCount, err := logEvent()

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	payload := AppInfo{appName, appVersion, podId, apiKey, logCount}

	json, err := json.Marshal(payload)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(json)
}

func resetHandler(w http.ResponseWriter, r *http.Request) {
	err := resetLogEvents()

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(200)
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
}

func main() {
	podId = os.Getenv("POD_ID")
	apiKey = os.Getenv("API_KEY")

	if apiKey == "" {
		log.Fatal("env var API_KEY is undefined")
		return
	}

	http.HandleFunc("/", handler)
	http.HandleFunc("/reset", resetHandler)
	http.HandleFunc("/health", healthHandler)

	err := http.ListenAndServe(":8080", nil)

	if err != nil {
		log.Fatal(err)
		return
	}

	log.Println("Server running on port 8080")
}
